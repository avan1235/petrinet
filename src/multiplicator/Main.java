package multiplicator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    private static final int MULTIPLICATOR_THREADS = 4;

    public static void main(String... args) {

        final var scanner = new Scanner(System.in);
        System.out.print("Enter first value: ");
        final var fstValue = scanner.nextInt();
        System.out.print("Enter second value: ");
        final var sndValue = scanner.nextInt();

        if (fstValue < 0 || sndValue < 0) {
            throw new IllegalArgumentException("Values cannot be less than 0");
        }

        final String A = "A", B = "B", resultPlace = "R",
                     P1 = "P1", P2 = "P2", P3 = "P3", P4 = "P4";

        final var baseTransitions = Stream.of(
                Transition.withInput(A).withOutput(P1).withInhibitor(P2).withReset(P3, P4),
                Transition.withInput(P1, P3).withOutput(P2).withInhibitor(A),
                Transition.withInput(P2).withOutput(A, resultPlace).withInhibitor(P1),
                Transition.withInput(B).withOutput(P4).withInhibitor(A, P4),
                Transition.withInput(P4).withOutput(P3, P4).withInhibitor(P3)
        )
                .map(Transition::build)
                .collect(Collectors.toSet());

        final var finalTransition = Transition.withInhibitor(A, B, P2, P3, P4).build();

        final var net = PetriNet.getFairFor(
                Map.entry(A, Math.max(fstValue, sndValue)),
                Map.entry(B, Math.min(fstValue, sndValue)));

        final var threads = Stream
                .generate(() -> new NetFirer<>(net, baseTransitions))
                .limit(MULTIPLICATOR_THREADS)
                .collect(Collectors.toList());

        final var executor = Executors.newFixedThreadPool(threads.size());
        threads.forEach(executor::submit);
        try {
            net.fire(Set.of(finalTransition));
            System.out.println("Final place in multiplicator PetriNet fired -> result is " + PetriNet.markingGetter.apply(net.getCurrentMarking(), resultPlace) + ".");
        } catch (InterruptedException exc) {
            System.err.println("Main thread interrupted when calling fire for " + finalTransition);
        } finally {
            executor.shutdownNow();
        }
    }

    private static final class NetFirer<T> implements Runnable {

        private static int instanceCounter = 0;

        private final PetriNet<T> petriNet;
        private final Collection<Transition<T>> transitions;
        private final int id;

        private NetFirer(PetriNet<T> petriNet, Collection<Transition<T>> transitions) {
            this.petriNet = petriNet;
            this.transitions = transitions;
            this.id = instanceCounter++;
        }

        @Override
        public void run() {
            int howManyPlacesFired = 0;
            try {
                while (true) {
                    petriNet.fire(transitions);
                    howManyPlacesFired++;
                }
            } catch (InterruptedException exc) {
                System.out.println(this.toString() + " fired " + howManyPlacesFired + " place(s).");
            }
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + " number " + id;
        }
    }
}
