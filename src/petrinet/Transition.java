package petrinet;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Transition<T> {

    private final Map<T, Integer> input;
    private final Map<T, Integer> output;
    private final Set<T> reset;
    private final Set<T> inhibitor;

    /**
     * Construct new transition with defined arcs and places
     * @param input input arcs from places equal to input.keySet() with weight of arc from x equal to input.get(x)
     * @param reset reset arcs places collection
     * @param inhibitor inhibitor arcs places collection
     * @param output output arcs from places equal to output.keySet() with weight of arc from x equal to output.get(x)
     */
    public Transition(Map<T, Integer> input, Collection<T> reset, Collection<T> inhibitor, Map<T, Integer> output) {
        this.input = Collections.synchronizedMap(new HashMap<>(input));
        this.output = Collections.synchronizedMap(new HashMap<>(output));
        this.reset = Collections.synchronizedSet(new HashSet<>(reset));
        this.inhibitor = Collections.synchronizedSet(new HashSet<>(inhibitor));
    }

    /**
     * Check if there is <b>NOT</b> fulfilled condition that in the specified for output
     * function place there are more tokens than the weight of the input arc from this place
     * @param marking for which the condition is checked
     * @return function checking the specified condition
     */
    private Predicate<T> inputArcChecker(Map<T, Integer> marking) {
        return place -> PetriNet.markingGetter.apply(marking, place) < input.get(place);
    }

    /**
     * Check if there are <b>MORE</b> than zero tokens in the specified for output function
     * place for the place that is connected with the reset arc
     * @param marking for which condition is checked
     * @return function checking the specified condition
     */
    private Predicate<T> inhibitorArcChecker(Map<T, Integer> marking) {
        return place -> PetriNet.markingGetter.apply(marking, place) != 0;
    }

    /**
     * Remove from place number of tokens equal to the weight of the input
     * arc from this place
     * @param marking which is modified
     * @return function for removing tokens from places for marking
     */
    private Consumer<T> inputArcTokensRemover(Map<T, Integer> marking) {
        return place -> {
            var updatedValue = PetriNet.markingGetter.apply(marking, place) - input.get(place);
            if (updatedValue > 0) {
                marking.put(place, updatedValue); // update map value
            }
            else {
                marking.remove(place);
            }
        };
    }

    /**
     * Remove <b>ALL</b> tokens from places in marking function
     * @param marking which is modified
     * @return function for removing all tokens from places
     */
    private Consumer<T> resetArcTokensRemover(Map<T, Integer> marking) {
        return marking::remove;
    }

    /**
     * Add to place number of tokens equal to weight of the output arc
     * to the specified place
     * @param marking which is modified
     * @return function for adding tokens to places for marking
     */
    private Consumer<T> outputArcTokensAdder(Map<T, Integer> marking) {
        return place -> {
            var updatedValue = PetriNet.markingGetter.apply(marking, place) + output.get(place);
            if (updatedValue > 0) {
                marking.put(place, updatedValue);
            }
            else {
                marking.remove(place);
            }
        };
    }

    /**
     * Check if the transition is enabled. The transition is enabled if the number of tokens
     * connected with transition by input arcs is greater or equal to the weights of the arcs
     * connecting the input arcs with {@code this} transition and if the number of tokens in
     * places connected with transition by reset arcs is equal to zero.
     * @param marking for which the transition is checked
     * @return true when {@code this} transition enabled  for specified marking, otherwise false
     */
    public boolean isEnabled(Map<T, Integer> marking) {
        var inputArcsNegPredictor = inputArcChecker(marking);
        var inhibitorArcsNegPredictor = inhibitorArcChecker(marking);
        return input.keySet().stream().noneMatch(inputArcsNegPredictor) && inhibitor.stream().noneMatch(inhibitorArcsNegPredictor);
    }

    public Map<T, Integer> fire(Map<T, Integer> marking) {
        var markingAfterFire = new HashMap<>(marking); // work on copy of given marking

        var inputRemover = inputArcTokensRemover(markingAfterFire);
        var resetRemover = resetArcTokensRemover(markingAfterFire);
        var outputAdder = outputArcTokensAdder(markingAfterFire);

        input.keySet().forEach(inputRemover);
        reset.forEach(resetRemover);
        output.keySet().forEach(outputAdder);

        return markingAfterFire;
    }

    @Override
    public int hashCode() {
        return Objects.hash(input, output, inhibitor, reset);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Transition))
            return false;
        var other = (Transition<?>) o;
        return Objects.equals(this.input, other.input) && Objects.equals(this.output, other.output)
                && Objects.equals(this.inhibitor, other.inhibitor) && Objects.equals(this.reset, other.reset);
    }

    @SafeVarargs
    public static <T> Builder<T> withInput(T... inputPlaces) {
        return new Builder<T>().withInput(inputPlaces);
    }

    public static <T> Builder<T> withInput(Map<T, Integer> input) {
        return new Builder<T>().withInput(input);
    }

    @SafeVarargs
    public static <T> Builder<T> withOutput(T... outputPlaces) {
        return new Builder<T>().withInput(outputPlaces);
    }

    public static <T> Builder<T> withOutput(Map<T, Integer> output) {
        return new Builder<T>().withInput(output);
    }

    @SafeVarargs
    public static <T> Builder<T> withReset(T... reset) {
        return new Builder<T>().withReset(reset);
    }

    @SafeVarargs
    public static <T> Builder<T> withInhibitor(T... inhibitor) {
        return new Builder<T>().withInhibitor(inhibitor);
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static <T> Transition<T> build(Builder<T> builder) {
        return builder.build();
    }

    public static final class Builder<U> {

        private final Map<U, Integer> input;
        private final Map<U, Integer> output;
        private final Set<U> reset;
        private final Set<U> inhibitor;

        public Builder() {
            input = new HashMap<>();
            output = new HashMap<>();
            reset = new HashSet<>();
            inhibitor = new HashSet<>();
        }

        public Builder<U> withInput(Map<U, Integer> input) {
            this.input.putAll(input);
            return this;
        }

        @SafeVarargs
        public final Builder<U> withInput(U... inputPlaces) {
            for (U place : inputPlaces) {
                this.input.put(place, 1);
            }
            return this;
        }

        public final Builder<U> withOutput(Map<U, Integer> output) {
            this.output.putAll(output);
            return this;
        }

        @SafeVarargs
        public final Builder<U> withOutput(U... outputPlaces) {
            for (U place : outputPlaces) {
                this.output.put(place, 1);
            }
            return this;
        }

        @SafeVarargs
        public final Builder<U> withReset(U... reset) {
            this.reset.addAll(Arrays.asList(reset));
            return this;
        }

        @SafeVarargs
        public final Builder<U> withInhibitor(U... inhibitor) {
            this.inhibitor.addAll(Arrays.asList(inhibitor));
            return this;
        }

        public Transition<U> build() {
            return new Transition<>(input, reset, inhibitor, output);
        }
    }
}
