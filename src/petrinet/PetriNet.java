package petrinet;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class PetriNet<T> {

    /**
     * Function for getting number of tokens from marking - default option is that if the place not exists
     * in the marking then the number of tokens in this place is equal to zero
     */
    public static final BiFunction<Map<?, Integer>, Object, Integer> markingGetter = (marking, place) -> marking.getOrDefault(place, 0);


    /**
     * Current marking of net kept as the map from places of type T to number of tokens in these places
     */
    private final Map<T, Integer> currentMarking;

    /**
     * Defines if the PetriNet is waking the waiting threads in the order of suspending them (FIFO order)
     */
    private final boolean fair;

    /**
     * List of waiting threads for firing some transitions for marking in FIFO order
     */
    private final Queue<Semaphore> waitingQueue = new LinkedList<>();

    /**
     * Mutex for access to fire method in fair PetrNet
     */
    private final Semaphore mutex = new Semaphore(1, true);

    /**
     * Construct PetriNet with specified initial state and the priority defined by fair parameter.
     * @param initial in initial state. The place has non zero number of tokens only
     *                if it belongs to initial.keySet() where then initial.get(x)
     *                is the number of tokens in place x
     * @param fair the priority of waking threads waiting for execution of fire method
     *             is defined by this parameter
     */
    public PetriNet(Map<T, Integer> initial, boolean fair) {
        // create synchronized map to be used by different threads in reachable and fire methods
        this.currentMarking = Collections.synchronizedMap(
                initial.entrySet().stream()                     // filter zero tokens places for consistent structure
                        .filter(entry -> entry.getValue() > 0)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
        );
        this.fair = fair;
    }

    /**
     * Static method for fair PetriNet construction
     * @param initial marking of net
     * @return created fair PetriNet
     */
    @SafeVarargs
    public static <T> PetriNet<T> getFairFor(Map.Entry<T, Integer>... initial) {
        var marking = collectEntries(initial);
        return new PetriNet<>(marking, true);
    }

    /**
     * Static method for not fair PetriNet construction
     * @param initial marking of net
     * @return created not fair PetriNet
     */
    @SafeVarargs
    public static <T> PetriNet<T> getNotFairFor(Map.Entry<T, Integer>... initial) {
        var marking = collectEntries(initial);
        return new PetriNet<>(marking, false);
    }

    /**
     * Convert map entries into map by collecting their values
     * @param entries to be collected
     * @return map created from entries
     */
    @SafeVarargs
    private static <T> Map<T, Integer> collectEntries(Map.Entry<T, Integer>... entries) {
        var marking = new HashMap<T, Integer>();
        for (var entry : entries) {
            marking.put(entry.getKey(), entry.getValue());
        }
        return marking;
    }

    /**
     * Try to find all possible marking for the net that are reachable from current state of net after
     * zero or more changes from transitions state. If the set of reachable is finite then it is the result
     * of this method. Single map m that belongs to result set can be translated to marking only if
     * the place belongs to m.keySet() where m.get(x) is the number of tokes in place x
     * @param transitions collection of possible to use transitions
     * @return markings reachable from current state in net
     */
    public Set<Map<T, Integer>> reachable(Collection<Transition<T>> transitions) {
        final var workingOnCurrentMarking = new HashMap<>(currentMarking);
        final var resultMarkings = new HashSet<Map<T, Integer>>();
        resultMarkings.add(workingOnCurrentMarking);
        if (transitions.isEmpty()) {
            return resultMarkings;
        }

        final var work = new LinkedList<Map<T, Integer>>();
        work.add(workingOnCurrentMarking);

        while (!work.isEmpty()) {
            final var workMarking = work.poll();
            transitions.stream()
                    .filter(transition -> transition.isEnabled(workMarking))
                    .map(transition -> transition.fire(workMarking))
                    .filter(marking -> !resultMarkings.contains(marking))
                    .forEach(marking -> {
                        work.add(marking);
                        resultMarkings.add(marking);
                    });
        }
        return resultMarkings;
    }

    /**
     * Fire single enabled transition from given collection if there exist enabled transitions in current net.
     * If the net was created with fair = true then among suspended threads the longest waiting thread is chosen.
     * If none of transitions is enabled it interrupts the current thread and throws InterruptedException.
     * @param transitions not empty collection of transitions to choose from
     * @return the transition fired by this method if any fired
     * @throws InterruptedException on thread interruption
     */
    public Transition<T> fire(Collection<Transition<T>> transitions) throws InterruptedException {
        if (Objects.isNull(transitions) || transitions.isEmpty()) {
            throw new IllegalArgumentException("Collection of transitions cannot be null or empty");
        }
        return fair ? fireFair(transitions) : fireNotFair(transitions);
    }

    /*
     * Synchronized and not fair version of fire method which waits on this network
     * and tries to notify all threads when changes marking in PetriNet
     */
    private synchronized Transition<T> fireNotFair(Collection<Transition<T>> transitions) throws InterruptedException {
        var toFireTransition = findAnyEnabledForCurrentMarking(transitions);

        if (toFireTransition.isEmpty()) {
            do {
                this.wait();
                toFireTransition = findAnyEnabledForCurrentMarking(transitions);
                toFireTransition.ifPresent(this::fireUpdateAndNotifyAll);
            } while (toFireTransition.isEmpty());
        }
        else {
            fireUpdateAndNotifyAll(toFireTransition.get());
        }

        return toFireTransition.get();
    }

    private void fireUpdateAndNotifyAll(Transition<T> transition) {
        var updatedMarking = transition.fire(currentMarking);
        currentMarking.clear();
        currentMarking.putAll(updatedMarking);
        this.notifyAll();
    }

    private void fireUpdateAndReleaseFirst(Transition<T> transition, Semaphore... toBeRemoved) {
        var updatedMarking = transition.fire(currentMarking);
        currentMarking.clear();
        currentMarking.putAll(updatedMarking);

        for (var semaphore : toBeRemoved) {
            waitingQueue.remove(semaphore);
        }

        if (waitingQueue.isEmpty()) {
            mutex.release();
        }
        else {
            waitingQueue.peek().release(); // release first in waiting list queue
        }
    }

    private void findAndReleaseNextThreadInQueue(Semaphore waitingThread) {
        boolean findCurrent = false;
        for (var waiter : waitingQueue) {
            if (findCurrent) {
                waiter.release();
                break;
            }
            if (waiter.equals(waitingThread)) {
                findCurrent = true;
            }
        }
    }

    private Transition<T> fireFair(Collection<Transition<T>> transitions) throws InterruptedException {
        var mutexAcquired = false;          // special case interruption variable for logging mutex state
        final var waitingThread = new Semaphore(0);
        try {
            mutex.acquire();
            mutexAcquired = true;
            var toFireTransition = findAnyEnabledForCurrentMarking(transitions);

            if (toFireTransition.isEmpty()) {
                waitingQueue.add(waitingThread);
                do {
                    mutex.release();
                    mutexAcquired = false;
                    waitingThread.acquire();
                    mutexAcquired = true; // critical section inheritance case

                    toFireTransition = findAnyEnabledForCurrentMarking(transitions);
                    toFireTransition.ifPresentOrElse(
                            transition -> fireUpdateAndReleaseFirst(transition, waitingThread),
                            () -> findAndReleaseNextThreadInQueue(waitingThread));
                } while (toFireTransition.isEmpty());

            }
            else {
                fireUpdateAndReleaseFirst(toFireTransition.get());
            }
            return toFireTransition.get();
        }
        catch (InterruptedException exc) {
            if (mutexAcquired) {
                mutex.release();
            }
            waitingQueue.remove(waitingThread);
            throw exc;
        }
    }

    private boolean isEnabledForCurrentNetMarking(Transition<T> transition) {
        return transition.isEnabled(currentMarking);
    }

    private Optional<Transition<T>> findAnyEnabledForCurrentMarking(Collection<Transition<T>> transitions) {
        return transitions.stream()
                .filter(this::isEnabledForCurrentNetMarking)
                .findAny();
    }

    /**
     * Get copy map of current marking in PetriNet. If some place has 0 tokens in current PetriNet state
     * than it is not included into the keySet of the marking map.
     * @return map from places to number of tokens
     */
    public Map<T, Integer> getCurrentMarking() {
        return new HashMap<>(currentMarking);
    }
}
