package alternator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {

    private static final int MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES = 1;
    private static final long WORK_TIME_MILLIS = 30 * 1000;

    public static void main(String... args) {

        final String critic = "Z", A = "A", B = "B", C = "C",
                     hadA = "hadA", hadB = "hadB", hadC = "hadC";

        final var transitionBeginA = Transition.withInput(critic).withOutput(A).withInhibitor(hadA).build();
        final var transitionFinishA = Transition.withInput(A).withOutput(critic, hadA).withReset(hadB, hadC).build();

        final var transitionBeginB = Transition.withInput(critic).withOutput(B).withInhibitor(hadB).build();
        final var transitionFinishB = Transition.withInput(B).withOutput(critic, hadB).withReset(hadC, hadA).build();

        final var transitionBeginC = Transition.withInput(critic).withOutput(C).withInhibitor(hadC).build();
        final var transitionFinishC = Transition.withInput(C).withOutput(critic, hadC).withReset(hadA, hadB).build();

        final var transitions = Set.of(
                transitionBeginA, transitionFinishA,
                transitionBeginB, transitionFinishB,
                transitionBeginC, transitionFinishC);

        /*
         * PetriNet in which at most single process can be in critical section (so the place A, B or C then has token) and
         * in which the network remembers which place had critical section by keeping token in special places had* (where * is A, B or C).
         */
        final var net = PetriNet.getFairFor(
                Map.entry(critic, 1));      // single token that is used by critical section or can be taken by some process


        final var markings = net.reachable(transitions);
        System.out.println(markings.size() + " markings are reachable from initial state.");

        final var isNotSecure = safetyChecker(false, critic, A, B, C); // can be changed to true to display logs in checking
        final var invalidMarkings = markings.stream().filter(isNotSecure).count();
        System.out.println(invalidMarkings + " marking(s) in alternator PetriNet are invalid due to security check.");

        final var threads = List.of(
                new TestPrinter<>(A, net, transitionBeginA, transitionFinishA),
                new TestPrinter<>(B, net, transitionBeginB, transitionFinishB),
                new TestPrinter<>(C, net, transitionBeginC, transitionFinishC));

        final var executor = Executors.newFixedThreadPool(threads.size());
        threads.forEach(executor::submit);

        try {
            Thread.sleep(WORK_TIME_MILLIS);
        } catch (InterruptedException exc) {
            System.err.println("Main interrupted. Executor will be shutdown now.");
        } finally {
            executor.shutdownNow();
        }
    }

    /**
     * Predicate creator for checking if the specified marking in PetriNet is safe
     * @param printLogs true when logs should be printed in checking time, otherwise false
     * @param places collection of places in which the number of tokens is summed and cannot be higher
     *               that the MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES
     * @return predicate for safety markings in net
     */
    @SafeVarargs
    private static <T> Predicate<Map<T, Integer>> safetyChecker(boolean printLogs, T... places) {
        return marking -> {
            final int sumInCriticalPlaces = Arrays.stream(places)                   // sum the number of tokens in all places in net
                    .mapToInt(place -> PetriNet.markingGetter.apply(marking, place))  // then compare to MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES
                    .sum();                                                         // to check safety in network

            if (!printLogs) {
                return sumInCriticalPlaces > MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES;
            }

            if (sumInCriticalPlaces > MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES) {
                System.err.println("LOG: Marking " + marking.toString() + " has security error");
                final var conflictPlaces = Arrays.stream(places)
                        .filter(place -> PetriNet.markingGetter.apply(marking, place) > 0)
                        .map(Object::toString)
                        .collect(Collectors.joining(", ", "", ""));
                System.err.println("Conflict places are: " + conflictPlaces);
            }
            else {
                System.err.println("LOG: Marking " + marking.toString() + " is secure");
            }
            return sumInCriticalPlaces > MAX_TOKENS_NUMBER_IN_CRITICAL_PLACES;
        };
    }

    private static final class TestPrinter<T> extends Thread {

        private final PetriNet<T> petriNet;
        private final Collection<Transition<T>> transitionBegin;
        private final Collection<Transition<T>> transitionFinish;

        private TestPrinter(String name, PetriNet<T> petriNet, Transition<T> transitionBegin, Transition<T> transitionFinish) {
            super(name);
            this.petriNet = petriNet;
            this.transitionBegin = Collections.singleton(transitionBegin);
            this.transitionFinish = Collections.singleton(transitionFinish);
        }

        @Override
        public void run() {
            try {
                while (true) {
                    petriNet.fire(transitionBegin);
                    System.out.print(this.getName());
                    System.out.print(".");
                    petriNet.fire(transitionFinish);
                }
            } catch (InterruptedException exc) {
                // thread interrupted by executor at the end of its work
                // no message written to output
            }
        }
    }
}
